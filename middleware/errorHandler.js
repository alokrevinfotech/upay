const { logEvents } = require('./logEvents');

const errorHandler = (err, req, res, next) => {
    logEvents(`${err.name}: ${err.message}`, 'errLog.txt').then(() => {
        console.error(err.stack)
        res.status(500).send(err.message);
    }).catch(err => {
        console.log(err);
        res.status(500).send(err);
    });
}

module.exports = errorHandler;