const multer = require('multer');
const path = require("path");

const profileStorageEngine = multer.diskStorage({
    destination: function (req, file, cb) {
        cb(null, path.join(__dirname, './../public/media/users/avatar'));
    },
    filename: function (req, file, cb) {
        cb(null, Date.now()+'-'+req.uid+'-'+file.originalname);
    }
})


const userAvatar = multer({storage: profileStorageEngine});

module.exports = {
    userAvatar
};