const jwt = require('jsonwebtoken');

const verifyJWT = (req, res, next) => {
    const authHeader = req.headers.authorization || req.headers.Authorization;
    if (!authHeader?.startsWith('Bearer ')) return res.status(401).json({"success":false, "message":"Bearer token is invalid"});
    const token = authHeader.split(' ')[1];
    jwt.verify(
        token,
        process.env.JWT_SECRET_KEY,
        (err, decoded) => {
            if (err) return res.status(401).json({"success":false, "message":"Token has been expired"}); //invalid token
            req.uid = decoded.UserInfo.id;
            req.roles = decoded.UserInfo.roles;
            req.store_id = decoded.UserInfo.store_id;
            next();
        }
    );
}

module.exports = verifyJWT