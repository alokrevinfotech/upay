require('dotenv').config()
const {sendErrorResponse, sendSuccessResponse} = require('./../helpers/response')
const {OAuth2Client} = require('google-auth-library');
const client = new OAuth2Client(process.env.GOOGLE_CLIENT_ID, process.env.GOOGLE_SECRET_ID, process.env.GOOGLE_REDIRECT_URL);
const SCOPES = [
    'https://www.googleapis.com/auth/youtube',
    'https://www.googleapis.com/auth/youtubepartner',
    'https://www.googleapis.com/auth/yt-analytics-monetary.readonly',
    'https://www.googleapis.com/auth/yt-analytics.readonly'
]

const fetchAccounts = (req, res) => {
    const url = client.generateAuthUrl({
        access_type: 'offline',
        scope: SCOPES
    })
    return sendSuccessResponse(res, 200, 'Testing', url);
}

module.exports = {
    fetchAccounts
}