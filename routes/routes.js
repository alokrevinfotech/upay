const express = require('express');
const router = express.Router();
const controller = require('./../controllers/controller');

router.route('/accounts').get(controller.fetchAccounts);

module.exports = router;