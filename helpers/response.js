const sendErrorResponse = (res, code=403, message='Something Went Wrong', data=null) => res.status(code).json({"success": false, "message": message, "data": data});
const sendSuccessResponse = (res, code=200, message='Successfully Completed', data=null) => res.status(code).json({"success": true, "message": message, "data": data});

module.exports = {
    sendErrorResponse,
    sendSuccessResponse
}