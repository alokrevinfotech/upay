const allowedOrigins = [
    'https://www.upay.com',
    'http://127.0.0.1:8090',
    'http://localhost:8090'
];

module.exports = allowedOrigins;